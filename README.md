# Module Usage

## Dependencies:
  - jquery_ui_accordion

## To use:

- Click "Extend" on the admin menu (/admin/modules).
- Click on the description accordion of any module in the list.
- The new "Module Usage Documentation" accordion will display in the module description pane.

-or-

- Click on Extend -> Update on the admin menu (/admin/modules/update).
- Click on the new "Module Usage Documentation" accordion in the desired module row.

Use the forms to add a general description of how/why the module is being used, document URLs where the module
functionality can be seen/tested and procedures for testing, add notes about modules such as version requirements.

Module history can be viewed on the Activity tab which will show dates that the module was installed/uninstalled and
when version changes occur.  Note:  When module_usage is initially installed, there is no way of determining the
installation dates of other modules so an activity record is added to each module at that time with an event name of:
"Initial Discovery".  Subsequent installs/uninstalls will be captured and recorded.

## Additional Features
- Views integration is provided.

- Integration with the module_filter module is supported subject to this issue being resolved:
  https://www.drupal.org/project/module_filter/issues/3418465#comment-15426218

- Import/Export functionality is provided at:  Extend -> Documentation -> Import (or Export)
- - Export will create and download a JSON file containing all module usage data
- -  Import will read the exported JSON file and (re)-create all the records.  Existing records will be overwritten.

- Alternatively, the Migration module might be useful to transfer data between dev/stage/prod environments.
  There is a sample migrations folder in the DEV branch with configurations for each table.  These are currently un-tested.
- Drush commands to perform import/export operations
## Other notes
- Uninstalling this module will delete all of its tables resulting in loss of all data. If there is any intention of
re-installing and continuing to use it, an export file should be created that can then be used to re-populate the data.
