<?php

namespace Drupal\module_usage\Services;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;

/**
 * The Module Usage service.
 */
class ModuleUsageService {

  /**
   * The logger.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private LoggerChannelFactoryInterface $logger;

  const DEFAULT_DATE_FORMAT = 'Y-m-d';
  const CRON_STATE_NAME = 'module_usage.last_cron_run';
  const CRON_RUN_INTERVAL = 900;

  /**
   * The Module Extension List service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  private ModuleExtensionList $moduleExtensionList;

  /**
   * The current date in timestamp format.
   *
   * @var int
   */
  private int $currentTS;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  private Renderer $renderer;

  /**
   * The state service.
   *
   * @var \Drupal\workflows\StateInterface
   */
  private StateInterface $state;

  /**
   * The Query service.
   *
   * @var \Drupal\module_usage\Services\QueryService
   */
  private QueryService $queryService;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The Module Extension List service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\module_usage\Services\QueryService $queryService
   *   The query service.
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger,
    ModuleExtensionList $moduleExtensionList,
    Renderer $renderer,
    StateInterface $state,
    QueryService $queryService,
  ) {

    $this->logger = $logger;
    $this->moduleExtensionList = $moduleExtensionList;

    $default_timezone = \Drupal::config('system.date')->get('timezone.default');
    $current_date = new DrupalDateTime('now', new \DateTimeZone($default_timezone));
    $this->currentTS = $current_date->getTimestamp();
    $this->renderer = $renderer;
    $this->state = $state;
    $this->queryService = $queryService;
  }

  /**
   * Creates a render array for displaying a module.
   *
   * @param \stdClass $module
   *   The module object.
   *
   * @return array
   *   A render array to display the module.
   *
   * @throws \Exception
   */
  public function getModuleView(\stdClass $module) {

    $machineName = $module->machine_name;

    return [
      '#theme' => 'module_accordion',
      '#item' => $module,
      '#description' => [
        '#type' => 'processed_text',
        '#text' => (!empty($module->description)) ? $module->description : 'How/Why/Where this module is used on this site, or any other info to be provided at-a-glance.',
        '#format' => $module->text_format,
      ],
      '#activity' => $this->queryService->getActivity($machineName),
      '#notes' => $this->getModuleNotes($machineName),
      '#urls' => $this->getModuleUrls($machineName),
    ];
  }

  /**
   * Retrieve module activity.
   *
   * @param string $machineName
   *   The module machine name.
   *
   * @return array
   *   The activity records.
   */
  public function getModuleActivity(string $machineName) {
    return $this->queryService->getActivity($machineName);
  }

  /**
   * Retrieve module notes.
   *
   * @param string $machineName
   *   The module machine name.
   *
   * @return array
   *   A render array of notes.
   */
  public function getModuleNotes(string $machineName) : array {
    $renderArray = [];
    $notes = $this->queryService->getNotes($machineName);
    foreach ($notes as $id => $note) {
      $renderArray[] = [
        'id' => $note->id,
        'created' => $note->created,
        'changed' => $note->changed,
        'note_title' => $note->note_title,
        'note_text' => [
          '#type' => 'processed_text',
          '#text' => $note->note_text,
          '#format' => $note->text_format,
        ],
      ];
    }
    return $renderArray;
  }

  /**
   * Retrieve module urls.
   *
   * @param string $machineName
   *   The module machine name.
   *
   * @return array
   *   The url records.
   */
  public function getModuleUrls(string $machineName) {
    $renderArray = [];
    $urls = $this->queryService->getUrls($machineName);
    foreach ($urls as $item) {

      $url = $item->url;

      $renderArray[] = [
        'id' => $item->id,
        'url' => [
          '#type' => 'link',
          '#url' => Url::fromUri('internal:' . $url),
          '#title' => $url,
        ],
        'url_description' => [
          '#type' => 'processed_text',
          '#text' => $item->url_description,
          '#format' => $item->text_format,
        ],
      ];
    }
    return $renderArray;
  }

  /**
   * Adds a note to a module usage entity.
   *
   * @param string $machineName
   *   The module name.
   * @param array $info
   *   The module field values array.
   * @param string $event
   *   Description of the event that took place.
   *
   * @throws \Exception
   */
  public function addModule(
    string $machineName,
    Extension $info,
    string $event = 'Initial Discovery') : void {

    $params = [
      'name' => $info->info['name'],
      'package' => $info->info['package'],
      'version' => $info->info['version'] ?? 'unknown',
      'status' => $info->status,
    ];
    $this->queryService->addModule($machineName, $params);

    $activity = [
      'date' => $this->currentTS,
      'version' => $info->info['version'] ?? 'unknown',
      'status' => $info->status,
      'description' => $event,
    ];

    $this->queryService->addActivity($machineName, $activity);
  }

  /**
   * Processes list of modules that were [un]installed.
   *
   * @param array $modules
   *   THe list of modules.
   * @param string $status
   *   The status - enabled|disabled.
   * @param string $event
   *   The event description.
   *
   * @return array
   *   A list of modules.
   */
  private function doEvents(array $modules, string $status, string $event): array {

    $modulesFound = [];

    try {
      $modules = $this->queryService->getModules($modules);
    }
    catch (\Exception $e) {
      return [];
    }

    if (!empty($modules)) {
      // Modules that were previously installed.
      foreach ($modules as $moduleObj) {
        $module = $moduleObj->machine_name;
        $moduleInfo = $this->moduleExtensionList->get($module);
        $version = ($moduleInfo && !empty($moduleInfo->info['version'])) ? $moduleInfo->info['version'] : 'unknown';

        $this->addEvent($module, $event, $status, $version);
        $modulesFound[] = $module;
      }
    }

    return $modulesFound;
  }

  /**
   * Create module usage entities for new modules.
   *
   * @param array $modules
   *   The list of modules that were installed.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function modulesInstalled(array $modules): void {

    $existingModules = $this->doEvents($modules, 1, 'Installed');

    foreach ($modules as $module) {
      if (!in_array($module, $existingModules)) {
        $moduleInfo = $this->moduleExtensionList->get($module);
        $this->addModule($module, $moduleInfo, 'Installed');
      }
    }
  }

  /**
   * Add events for modules that were uninstalled.
   *
   * @param array $modules
   *   The list of modules that was uninstalled.
   */
  public function modulesUninstalled(array $modules): void {
    $this->doEvents($modules, 0, 'Uninstalled');
  }

  /**
   * Create an event for a module usage entity.
   *
   * @param string $machineName
   *   The module machine name.
   * @param string $event
   *   The event description.
   * @param int $status
   *   The module status.
   * @param string $version
   *   The module version.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addEvent(string $machineName, string $event, int $status, string $version) {

    $eventInfo = [
      'description' => $event,
      'date' => $this->currentTS,
      'status' => $status,
      'version' => $version,
    ];

    $this->queryService->addActivity($machineName, $eventInfo);
  }

  /**
   * Delete a URL from a module usage entity.
   *
   * @param int $urlId
   *   The URL ID.
   *
   * @return bool
   *   Success or failure status
   */
  public function deleteUrl(int $urlId) : bool {
    return $this->queryService->deleteUrl($urlId);
  }

  /**
   * Delete a note from a module usage entity.
   *
   * @param int $id
   *   The Note id.
   *
   * @return bool
   *   Success or failure status.
   */
  public function deleteNote(int $id) {
    return $this->queryService->deleteNote($id);
  }

  /**
   * Retrieves a module usage entity by machine name.
   *
   * @param string $machineName
   *   The module name.
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|\Drupal\module_usage\Entity\ModuleUsage|null
   *   A module usage entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getModuleByMachineName(string $machineName) {
    return $this->queryService->getModule($machineName);
  }

  /**
   * Renders a module.
   *
   * @param string $machine_name
   *   The module machine name.
   *
   * @return \Drupal\Component\Render\MarkupInterface|mixed|string|string[]|null
   *   The rendered output.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function renderModule(string $machine_name) {
    $entity = $this->getModuleByMachineName($machine_name);
    if (!$entity) {
      return [
        '#markup' => '<div>There was an error retrieving the information for this module</div>',
      ];
    }
    $renderArray = $this->getModuleView($entity);
    return $this->renderer->render($renderArray);
  }

  /**
   * Adds a URL to a module.
   *
   * @param string $machine_name
   *   The module machine name.
   * @param string $url
   *   The URL.
   * @param string $notes
   *   The notes.
   * @param string $format
   *   The text format.
   *
   * @return int|Null
   *   The id of the new record.
   */
  public function addUrl(string $machine_name, string $url, string $notes, string $format) : int | Null {
    $entity = $this->getModuleByMachineName($machine_name);
    if ($entity) {
      $urlInfo = [
        'url' => $url,
        'description' => $notes,
        'text_format' => $format,
      ];
      return $this->queryService->addUrl($machine_name, $urlInfo);
    }

    return NULL;
  }

  /**
   * Retrieves a URL record.
   *
   * @param int $id
   *   The id of the record to retrieve.
   *
   * @return mixed
   *   The URL record
   */
  public function getUrl($id) {
    return $this->queryService->getUrl($id);
  }

  /**
   * Retrieves a Note record.
   *
   * @param int $id
   *   The id of the record to retrieve.
   *
   * @return mixed
   *   The Note record
   */
  public function getNote($id) {
    return $this->queryService->getNote($id);
  }

  /**
   * Updates a url record.
   *
   * @param int $id
   *   The URL ID.
   * @param string $url
   *   The URL.
   * @param array $notes
   *   The notes.
   *
   * @return int|Null
   *   The status.
   */
  public function editUrl(int $id, string $url, array $notes) : int | Null {
    $params = [
      'url' => $url,
      'description' => $notes['value'],
      'text_format' => $notes['format'],
    ];
    return $this->queryService->updateUrl($id, $params);
  }

  /**
   * Updates a note record.
   *
   * @param int $id
   *   The Note ID.
   * @param array $notes
   *   The notes.
   * @param string $note_title
   *   The note title.
   *
   * @return int|Null
   *   The status.
   */
  public function editNote(int $id, array $notes, string $note_title) : int | Null {
    $params = [
      'note_text' => $notes['value'],
      'text_format' => $notes['format'],
      'note_title' => $note_title,
    ];
    return $this->queryService->updateNote($id, $params);
  }

  /**
   * Updates the description value of a module usage entity.
   *
   * @param string $machine_name
   *   The module machine name.
   * @param string $description
   *   The module description.
   * @param string $format
   *   The text format.
   *
   * @return int
   *   The number of rows that were updated.
   */
  public function saveDescription(string $machine_name, string $description, string $format) {
    return $this->queryService->saveDescription($machine_name, $description, $format);
  }

  /**
   * Adds a note to a module usage entity.
   *
   * @param string $machine_name
   *   The machine name of the module.
   * @param array $note
   *   The note.
   * @param string $note_title
   *   The note title.
   *
   * @return \stdClass|Null
   *   The module usage object.
   */
  public function addNote(string $machine_name, array $note, string $note_title) : mixed {
    $entity = $this->getModuleByMachineName($machine_name);
    $now = new \DateTime();
    if ($entity) {
      $noteInfo = [
        'date' => $now->getTimestamp(),
        'description' => $note['value'],
        'text_format' => $note['format'],
        'note_title' => $note_title,
      ];

      $this->queryService->addNote($machine_name, $noteInfo);
      return $entity;
    }

    return NULL;
  }

  /**
   * Determines whether or not it's time to run the cron job.
   */
  public function cronRunNeeded() : bool {
    $last_run = $this->state->get(self::CRON_STATE_NAME, 0);
    $current_time = \Drupal::time()->getRequestTime();

    // Check if the interval has elapsed since the last run.
    return (($current_time - $last_run) > self::CRON_RUN_INTERVAL);
  }

  /**
   * Save cron run time in state.
   */
  public function cronRan() : void {
    $current_time = \Drupal::time()->getRequestTime();
    $this->state->set(self::CRON_STATE_NAME, $current_time);
  }

  /**
   * Scan all modules and check for version changes.
   */
  public function detectVersionChanges() : void {
    $moduleInfo = $this->moduleExtensionList->reset()->getList();
    $modules = $this->queryService->getModules();

    foreach ($modules as $module) {
      $machine_name = $module->machine_name;
      $version = $module->version;

      if (!isset($moduleInfo[$machine_name])) {
        continue;
      }

      $info = $moduleInfo[$machine_name];

      if (($info->info['version'] ?? 'unknown') != $version) {
        $this->addEvent($machine_name, 'Version changed from ' . $version, $info->status, $info->info['version'] ?? 'unknown');
        $this->queryService->updateVersion($machine_name, $info->info['version'] ?? 'unknown');
      }
    }
  }

  /**
   * Renders a URL field.
   *
   * @param string $machineName
   *   The module machine name.
   *
   * @return \Drupal\Component\Render\MarkupInterface|mixed|string|null
   *   The rendered output.
   *
   * @throws \Exception
   */
  public function renderUrls(string $machineName) {
    $items = $this->getModuleUrls($machineName);
    $render_array = [
      '#theme' => 'module_usage__urls',
      '#urls' => $items,
      '#machine_name' => $machineName,
      '#attributes' => [],
    ];
    return $this->renderer->render($render_array);
  }

  /**
   * Renders a notes field.
   *
   * @param string $machineName
   *   The module machine name.
   *
   * @return \Drupal\Component\Render\MarkupInterface|mixed|string|null
   *   The rendered output.
   *
   * @throws \Exception
   */
  public function renderNotes(string $machineName) {
    $items = $this->getModuleNotes($machineName);
    $render_array = [
      '#theme' => 'module_usage__notes',
      '#notes' => $items,
      '#machine_name' => $machineName,
    ];
    return $this->renderer->render($render_array);
  }

  /**
   * Renders a description field.
   *
   * @param string $machineName
   *   The module machine name.
   *
   * @return \Drupal\Component\Render\MarkupInterface|mixed|string|null
   *   The rendered output.
   *
   * @throws \Exception
   */
  public function renderDescription(string $machineName) : mixed {
    $module = $this->queryService->getModule($machineName);
    $desc = $module?->description ?? '';
    $render_array = [
      '#theme' => 'module_usage__description',
      '#machine_name' => $machineName,
      '#description' => $desc,
      '#description_last_updated' => $module->description_last_updated ?? '',
    ];
    return $this->renderer->render($render_array);
  }

  /**
   * Exports the contents of module_usage tables to a JSON file.
   */
  public function exportModuleUsageData($modules = [], $encoded = TRUE) : string|array {
    // Define the tables to be exported.
    $tables = [
      'module_usage',
      'module_usage_activity',
      'module_usage_urls',
      'module_usage_notes',
    ];

    // Prepare an array to hold data from all tables.
    $data = [];

    // Fetch data from each table.
    foreach ($tables as $table) {
      $query = $this->queryService->getAll($table, $modules);

      // Fetch all records from the current table and add them to the array.
      while ($record = $query->fetchAssoc()) {
        $data[$table][] = $record;
      }
    }

    // Convert the data array to JSON.
    return ($encoded) ? Json::encode($data) : $data;

  }

}
