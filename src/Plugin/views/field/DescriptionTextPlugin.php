<?php

namespace Drupal\module_usage\Plugin\views\field;

use Drupal\module_usage\Services\QueryService;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A handler to provide a field that renders text from a custom table.
 */

/**
 * Description text plugin class.
 *
 * @ViewsField("module_usage_text_field")
 */
class DescriptionTextPlugin extends FieldPluginBase {
  /**
   * The database connection.
   *
   * @var \Drupal\module_usage\Services\QueryService
   */
  protected $database;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, QueryService $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_usage.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    // Add the field.
    $params = $this->options['group_type'] != 'group' ? ['function' => $this->options['group_type']] : [];
    $this->field_alias = $this->query->addField($this->tableAlias, $this->realField, NULL, $params);
    $this->query->addField($this->tableAlias, 'text_format', NULL, $params);

    $this->addAdditionalFields();
  }

  /**
   * Renders the output of the field.
   */
  public function render(ResultRow $values, $field = NULL) {

    $textAndFormat = $this->getTextAndFormat($values);
    // Check and format the data as required.
    if (!empty($textAndFormat)) {
      // If your text data includes HTML and you want to process text formats:
      return [
        '#type' => 'processed_text',
        '#text' => $textAndFormat['text'],
        '#format' => $textAndFormat['format'],
      ];
    }

    return [];
  }

  /**
   * Extract text and format from row.
   *
   * @param \Drupal\views\ResultRow $values
   *   The row from the view.
   *
   * @return array
   *   An array containing the text and format values.
   */
  private function getTextAndFormat(ResultRow $values): array {
    $value = $this->getValue($values);

    if (!empty($value)) {

      switch ($this->realField) {
        case 'description':
          $format = $values->module_usage_text_format;
          break;

        case 'url_description':
          $format = $values->module_usage_urls_module_usage_text_format;
          break;

        case 'note_text':
          $format = $values->module_usage_notes_module_usage_text_format;
          break;

        default:
          $format = 'plain_text';
      }

      return [
        'text' => $value,
        'format' => $format,
      ];
    }

    return [];
  }

  /**
   * Fetches the custom data based on the row information.
   */
  protected function getCustomData(ResultRow $values) {
    // Logic to retrieve data from your custom table based on the row data.
    // This might involve a database query using the Drupal::database service.
    // Example (you will need to adjust this to your specific use case):
    return $this->database->getModule($values->machine_name);
  }

}
