<?php

namespace Drupal\module_usage\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The Edit Description form.
 */
class EditDescriptionForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_edit_url_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $buildInfo = $form_state->getBuildInfo();
    $machine_name = $buildInfo['args'][0];
    $module = \Drupal::service('module_usage.usage_service')->getModuleByMachineName($machine_name);
    if (!$module) {
      return [
        '#markup' => t('There was an error'),
      ];
    }

    $formats = filter_formats();
    $default_format = ($formats) ? key($formats) : '';
    $text_format = $module->text_format ?? $default_format;

    $description = $module->description ?? '';

    $form['description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description'),
      '#format' => $text_format,
      '#default_value' => $description,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#ajax' => [
        'callback' => '::submitAjaxForm',
        'wrapper' => 'field-description-' . $machine_name,
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.ajax';
    $form['#attached']['library'][] = 'core/jquery.form';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['#token'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Handle form submission for non-AJAX submission.
  }

  /**
   * AJAX form submission handler.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormState object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AJAX response.
   */
  public function submitAjaxForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $desc = $form_state->getValue('description');

    $buildInfo = $form_state->getBuildInfo();
    $machineName = $buildInfo['args'][0] ?? '';

    /**
      * @var \Drupal\module_usage\Services\ModuleUsageService $service
      */
    $service = \Drupal::service('module_usage.usage_service');
    $count = NULL;

    if ($machineName) {
      $count = $service->saveDescription($machineName, $desc['value'], $desc['format']);
    }

    if ($count) {
      $content = $service->renderDescription($machineName);

      $response->addCommand(new ReplaceCommand('#field-description-' . $machineName, $content));
      $response->addCommand(new CloseModalDialogCommand());
      $response->addCommand(new InvokeCommand('#field-description-' . $machineName, 'focus', []));

      return $response;
    }

    $response->addCommand(new MessageCommand('Unable to update module documentation'));
    return $response;
  }

}
