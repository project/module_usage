<?php

namespace Drupal\module_usage\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Recent module updates form.
 */
class RecentUpdatesForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'my_custom_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Check for existing form values, fallback to default dates if none found.
    $input = $form_state->getUserInput();
    $default_start_date = $input['start_date'] ?? new DrupalDateTime('-1 week');
    $default_end_date = $input['end_date'] ?? new DrupalDateTime('now');

    // Ensure default dates are DrupalDateTime objects for consistency.
    if (!$default_start_date instanceof DrupalDateTime) {
      $default_start_date = new DrupalDateTime($default_start_date);
    }
    if (!$default_end_date instanceof DrupalDateTime) {
      $default_end_date = new DrupalDateTime($default_end_date);
    }

    $startDate = $default_start_date->format('Y-m-d');
    $endDate = $default_end_date->format('Y-m-d');

    $form['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start date'),
    // Default to 1 week ago.
      '#default_value' => $startDate,
      '#required' => TRUE,
    ];

    // End date.
    $form['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End date'),
    // Default to today.
      '#default_value' => $endDate,
      '#required' => TRUE,
    ];

    // Add a submit button to your form.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    $data = \Drupal::service('module_usage.query')->getModuleVersionUpdates($startDate, $endDate);
    $form['results'] = [
      '#theme' => 'module_usage_report_recent',
      '#module_data' => $data,
      '#attached' => [
        'library' => 'module_usage/report',
      ],
    ];

    $form['#attached']['library'][] = 'module_usage/report';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Handle form submission if necessary.
    $form_state->setRebuild(TRUE);
  }

}
