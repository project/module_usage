(function ($, Drupal) {
    'use strict';

    Drupal.AjaxCommands.prototype.setCount = function (ajax, response, status) {

      var label, selector;
      switch (response.tabName) {
        case 'notes':
          label = 'Notes (' + response.count + ')';
          selector = '#notes-button-' + response.machineName;
          break;
        case 'url':
          label = 'Urls (' + response.count + ')';
          selector = '#url-button-' + response.machineName;
          break;
        default:
          return;
      }

      $(selector).html(label );
      if (response.count == 0) {
        $(selector).removeClass('hasItems');
      }
      else {
        $(selector).addClass('hasItems');
      }
    };

    Drupal.behaviors.moduleUsagePopup = {
        attach: function (context, settings) {

            $(once('add-url-click', '.add-url', context)).each(
                function () {
                    $(this).on(
                        'click', function (evt) {
                            evt.stopPropagation();
                            const module = $(this).data('module');
                            if (module) {
                                var ajaxSettings = {
                                    url: '/module_usage/add-url/' + module,
                                };
                                Drupal.ajax(ajaxSettings).execute();
                                const moduleName = $(this).data('module-name');
                            }
                        }
                    );
                }
            );

            $(once('add-note-click', '.add-note', context)).each(
                function () {
                    $(this).on(
                        'click', function (evt) {
                            evt.stopPropagation();
                            const module = $(this).data('module');
                            if (module) {
                                var ajaxSettings = {
                                    url: '/module_usage/add-note/' + module,
                                };
                                Drupal.ajax(ajaxSettings).execute();
                                const moduleName = $(this).data('module-name');
                            }
                        }
                    );
                }
            );

            $(once('edit-url-click', '.edit-url', context)).each(
                function () {
                    $(this).on(
                        'click', function (evt) {
                            evt.stopPropagation();
                            const module = $(this).data('module');
                            const urlid = $(this).data('urlid');
                            if (module) {
                                  var ajaxSettings = {
                                        url: '/module_usage/edit-url/' + module + '/' + urlid,
                                };
                                  Drupal.ajax(ajaxSettings).execute();
                                  const moduleName = $(this).data('module-name');
                            }
                        }
                    );
                }
            );

            $(once('delete-url-click', '.delete-url', context)).each(
                function () {
                    $(this).on(
                        'click', function (evt) {
                            evt.stopPropagation();
                            const module = $(this).data('module');
                            const pid = $(this).data('pid');
                            if (module && pid) {
                                if  (confirm('Are you sure?')) {
                                    var ajaxSettings = {
                                        url: '/module_usage/delete-url/' + module + '/' + pid,
                                    };
                                    Drupal.ajax(ajaxSettings).execute();
                                }
                            }
                        }
                    );
                }
            );

            $(once('edit-note-click', '.edit-note', context)).each(
                function () {
                    $(this).on(
                        'click', function (evt) {
                            evt.stopPropagation();
                            const module = $(this).data('module');
                            const pid = $(this).data('noteid');
                            if (module) {
                                var ajaxSettings = {
                                    url: '/module_usage/edit-note/' + module + '/' + pid,
                                };
                                Drupal.ajax(ajaxSettings).execute();
                                const moduleName = $(this).data('module-name');
                            }
                        }
                    );
                }
            );

            $(once('delete-note-click', '.delete-note', context)).each(
                function () {
                    $(this).on(
                        'click', function (evt) {
                            evt.stopPropagation();
                            const module = $(this).data('module');
                            const nid = $(this).data('noteid');
                            if (module && nid) {
                                if  (confirm('Are you sure?')) {
                                    var ajaxSettings = {
                                        url: '/module_usage/delete-note/' + module + '/' + nid,
                                    };
                                    Drupal.ajax(ajaxSettings).execute();
                                }
                            }
                        }
                    );
                }
            );

            $(once('edit-description-click', '.field--name-description span.btn, .edit-description span.btn', context)).each(
                function () {
                    $(this).on(
                        'click', function (evt) {
                            evt.stopPropagation();
                            const module = $(this).data('module');
                            if (module) {
                                var ajaxSettings = {
                                    url: '/module_usage/edit-description/' + module,
                                };
                                Drupal.ajax(ajaxSettings).execute();
                            }
                        }
                    );
                }
            );

        },
    };

    Drupal.behaviors.moduleUsageTabBehavior = {
        attach: function (context, settings) {
            $(once('tab-buttons', '.tab-buttons button', context)).each(
                function () {
                    $(this).on(
                        'click', function (evt) {
                            evt.stopPropagation();
                            evt.preventDefault();

                            var tabset = $(this).parents('div.tab-set');
                            var buttonContainer = $(this).parents('.tab-buttons');
                            $('button', buttonContainer).removeClass('active');
                            $(this).addClass('active');
                            $('.tab-content', tabset).hide();
                            var contentTab = $(this).data('tab');
                            $('.' + contentTab, tabset).show();
                        }
                    );
                }
            );
        }
    };

    /**
     * Adds a custom behavior.
     */
    Drupal.behaviors.moduleUsageBehavior = {
        attach: function (context, settings) {
            // Use 'context' to restrict the scope
            // This is important for AJAX-loaded content
          if (drupalSettings.moduleUsage && drupalSettings.moduleUsage.hasDocumentation) {
            var moduleList = Object.values(drupalSettings.moduleUsage.hasDocumentation);
            $(once('add-module-classes', 'tr.module-list__module', context)).each(function () {
              var module = $(this).data('drupal-selector').replace('edit-modules-', '').replace('-', '_');
              if (moduleList.includes(module)) {
                $(this).addClass('mu-has-doc');
              }
              else {
                $(this).addClass('mu-no-doc');
              }
            });
            $(once('module-filters', 'input[name="module-usage-filter"]', context)).each(function () {
              $(this).on('click', function () {

                switch ($(this).val()) {
                  case 'All':
                    $('.mu-no-doc, .mu-has-doc').removeClass('module-usage-hidden');
                    break;

                  case 'With':
                    $('.mu-has-doc').removeClass('module-usage-hidden');
                    $('.mu-no-doc').addClass('module-usage-hidden');
                    break;

                  case 'Without':
                    $('.mu-no-doc').removeClass('module-usage-hidden');
                    $('.mu-has-doc').addClass('module-usage-hidden');
                    break;
                }
              });
            });
          }


          $(once('module-accordion', '.module-accordion', context)).each(
              function () {
                  $(this).accordion(
                      {
                          header: '.module-accordion-header',
                          heightStyle: 'content',
                          active: false,
                          collapsible: true
                      }
                  );
              }
          );

          $(once('module-list-accordion', '.module-list-accordion', context)).each(
              function () {
                  $(this).accordion(
                      {
                          header: '.accordion-group-header',
                          heightStyle: 'content',
                          active: false,
                          collapsible: true
                      }
                  );
              }
          );

          // Mark containers that are already populated
          $('.module-accordion-body-container').each(
              function () {
                  if (!$(this).data('loaded') && $(this).find('.module-accordion-body').length > 0) {
                      $(this).attr('data-loaded', 1);
                  }
              }
          );

          $(once('open-module-accordion', '.module-list__module-details summary', context)).each(function () {
            $(this).on('click', function () {
              var module = $(this).parent().attr('id')
                .replace('edit-modules-', '')
                .replace('-enable-description', '')
                .replace(/-/g, '_');

              var accordion = $('.module-documentation', $(this).parent());
              $(accordion).accordion('option', 'active', 0);
            })
          });

          $(once('module-documentation-accordion', '.module-documentation', context)).each(
              function () {
                  $(this).accordion(
                      {
                          header: '.module-accordion-header',
                          heightStyle: 'content',
                          active: false,
                          collapsible: true,
                          activate: function (evt, ui) {
                                var panel = ui.newPanel;
                                var loaded = $(panel).data('loaded');
                              if (!loaded && panel.length > 0) {
                                  var module = $(panel).data('module');
                                  var ajaxSettings = {
                                      method: 'GET',
                                      url: '/admin/module_usage/' + module,
                                  };
                                  Drupal.ajax(ajaxSettings).execute();
                              }
                          }
                      }
                  );
              }
          )

          $(once('disabled-toggle', '#disabled-module-toggle', context)).each(
              function () {
                  const checkbox = $(this);
                  $(this).on(
                      'change', function () {
                          $('.module-disabled').each(
                              function () {
                                  // Show or hide disabled modules
                                  if (checkbox.is(':checked')) {
                                        $(this).show();
                                  }
                                  else {
                                      $(this).hide();
                                  }
                              }
                          );
                      }
                  );
              }
          );

        }
    };

})(jQuery, Drupal);
